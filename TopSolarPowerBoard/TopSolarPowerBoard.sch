EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title "Top Solar Power Board"
Date "2019-11-19"
Rev ""
Comp ""
Comment1 "CERN Open Hardware Licence v1.2 "
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L pspice:INDUCTOR Lx1
U 1 1 5DA37DAE
P 4375 2275
F 0 "Lx1" H 4375 2490 50  0000 C CNN
F 1 "33uH" H 4375 2399 50  0000 C CNN
F 2 "Inductor_SMD:L_Bourns_SRN6045TA" H 4375 2275 50  0001 C CNN
F 3 "~" H 4375 2275 50  0001 C CNN
F 4 "SRN6045TA-330M" H 4375 2275 50  0001 C CNN "MPN"
	1    4375 2275
	1    0    0    -1  
$EndComp
$Comp
L Device:R R3
U 1 1 5DA38730
P 3575 2700
F 0 "R3" H 3645 2746 50  0000 L CNN
F 1 "1k" H 3645 2655 50  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric" V 3505 2700 50  0001 C CNN
F 3 "~" H 3575 2700 50  0001 C CNN
F 4 "CRCW06031K00FKEA" H 3575 2700 50  0001 C CNN "MPN"
	1    3575 2700
	1    0    0    -1  
$EndComp
$Comp
L Device:C C4
U 1 1 5DA3F1AC
P 4725 3200
F 0 "C4" H 4840 3246 50  0000 L CNN
F 1 "100nF" H 4840 3155 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 4763 3050 50  0001 C CNN
F 3 "~" H 4725 3200 50  0001 C CNN
F 4 "C0603C104K8RACTU" H 4725 3200 50  0001 C CNN "MPN"
	1    4725 3200
	1    0    0    -1  
$EndComp
$Comp
L Device:CP1 CIN1
U 1 1 5DA44F3B
P 3175 2825
F 0 "CIN1" H 3290 2871 50  0000 L CNN
F 1 "33uF" H 3290 2780 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 3175 2825 50  0001 C CNN
F 3 "~" H 3175 2825 50  0001 C CNN
F 4 "C2012X5R1A336M125AC" H 3175 2825 50  0001 C CNN "MPN"
	1    3175 2825
	1    0    0    -1  
$EndComp
Connection ~ 3575 2275
Wire Wire Line
	3575 2275 4125 2275
Wire Wire Line
	3575 2275 3575 2525
Connection ~ 3575 2525
Wire Wire Line
	3575 2525 3575 2550
Wire Wire Line
	3575 2850 3575 2875
Wire Wire Line
	3175 2675 3175 2275
Connection ~ 3175 2275
Wire Wire Line
	3175 2275 3575 2275
$Comp
L Device:C CF1
U 1 1 5DA56418
P 6375 2575
F 0 "CF1" H 6490 2621 50  0000 L CNN
F 1 "1uF" H 6490 2530 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 6413 2425 50  0001 C CNN
F 3 "~" H 6375 2575 50  0001 C CNN
F 4 "C0603C105M8RACAUTO" H 6375 2575 50  0001 C CNN "MPN"
	1    6375 2575
	1    0    0    -1  
$EndComp
$Comp
L Device:R RF1
U 1 1 5DA574EC
P 6775 2425
F 0 "RF1" V 6568 2425 50  0000 C CNN
F 1 "1k" V 6659 2425 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric" V 6705 2425 50  0001 C CNN
F 3 "~" H 6775 2425 50  0001 C CNN
F 4 "CRCW06031K00FKEA" H 6775 2425 50  0001 C CNN "MPN"
	1    6775 2425
	0    1    1    0   
$EndComp
$Comp
L Device:R RF2
U 1 1 5DA57C07
P 6775 2725
F 0 "RF2" V 6568 2725 50  0000 C CNN
F 1 "1k" V 6659 2725 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric" V 6705 2725 50  0001 C CNN
F 3 "~" H 6775 2725 50  0001 C CNN
F 4 "CRCW06031K00FKEA" H 6775 2725 50  0001 C CNN "MPN"
	1    6775 2725
	0    1    1    0   
$EndComp
$Comp
L Device:R Rs1
U 1 1 5DA5817B
P 7175 2275
F 0 "Rs1" V 6968 2275 50  0000 C CNN
F 1 "20m" V 7059 2275 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric" V 7105 2275 50  0001 C CNN
F 3 "~" H 7175 2275 50  0001 C CNN
F 4 "RCWE060320L0FQEA" H 7175 2275 50  0001 C CNN "MPN"
	1    7175 2275
	0    1    1    0   
$EndComp
$Comp
L Device:C C2
U 1 1 5DA590A4
P 6275 3200
F 0 "C2" H 6390 3246 50  0000 L CNN
F 1 "1nF" H 6390 3155 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 6313 3050 50  0001 C CNN
F 3 "~" H 6275 3200 50  0001 C CNN
F 4 "C0603C102J8HACTU" H 6275 3200 50  0001 C CNN "MPN"
	1    6275 3200
	1    0    0    -1  
$EndComp
$Comp
L Device:R R1
U 1 1 5DA59A26
P 7625 2525
F 0 "R1" H 7695 2571 50  0000 L CNN
F 1 "1M_0.1%" H 7695 2480 50  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric" V 7555 2525 50  0001 C CNN
F 3 "~" H 7625 2525 50  0001 C CNN
F 4 "RN73H1JTTD1004B25" H 7625 2525 50  0001 C CNN "MPN"
	1    7625 2525
	1    0    0    -1  
$EndComp
$Comp
L Device:R R2
U 1 1 5DA5A041
P 7625 3200
F 0 "R2" H 7695 3246 50  0000 L CNN
F 1 "422k_0.1%" H 7695 3155 50  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric" V 7555 3200 50  0001 C CNN
F 3 "~" H 7625 3200 50  0001 C CNN
F 4 "RN73H1JTTD4223B25" H 7625 3200 50  0001 C CNN "MPN"
	1    7625 3200
	1    0    0    -1  
$EndComp
Wire Wire Line
	6375 2425 6625 2425
Connection ~ 6375 2425
Wire Wire Line
	5975 2625 6225 2625
Wire Wire Line
	6225 2625 6225 2725
Wire Wire Line
	6225 2725 6375 2725
Wire Wire Line
	6375 2725 6625 2725
Connection ~ 6375 2725
Wire Wire Line
	6925 2425 7025 2425
Wire Wire Line
	7025 2425 7025 2275
Wire Wire Line
	7025 2275 6225 2275
Connection ~ 7025 2275
Wire Wire Line
	6925 2725 7325 2725
Wire Wire Line
	7325 2725 7325 2275
Wire Wire Line
	7325 2275 7625 2275
Connection ~ 7325 2275
Wire Wire Line
	7625 2675 7625 2875
Wire Wire Line
	7625 2275 7625 2375
Connection ~ 7625 2275
Wire Wire Line
	7625 2275 8525 2275
Wire Wire Line
	8525 2700 8525 2275
Connection ~ 7625 2875
$Comp
L power:GND #PWR01
U 1 1 5DA41164
P 5475 3075
F 0 "#PWR01" H 5475 2825 50  0001 C CNN
F 1 "GND" H 5480 2902 50  0000 C CNN
F 2 "" H 5475 3075 50  0001 C CNN
F 3 "" H 5475 3075 50  0001 C CNN
	1    5475 3075
	1    0    0    -1  
$EndComp
$Comp
L lsf-kicad:SPV1040 U1
U 1 1 5DAAC902
P 5475 2525
F 0 "U1" H 5475 3040 50  0000 C CNN
F 1 "SPV1040" H 5475 2949 50  0000 C CNN
F 2 "Package_SO:TSSOP-8_4.4x3mm_P0.65mm" H 5475 1925 50  0001 C CNN
F 3 "https://www.st.com/resource/en/datasheet/spv1040.pdf" H 5575 1775 50  0001 C CNN
F 4 "SPV1040TTR" H 5475 2525 50  0001 C CNN "MPN"
	1    5475 2525
	1    0    0    -1  
$EndComp
Text Label 5975 2325 0    50   ~ 0
VOUT
Text Label 5975 2725 0    50   ~ 0
VCTRL
Text Label 5975 2425 0    50   ~ 0
ICTRL+
Text Label 5975 2625 0    50   ~ 0
ICTRL-
Text Label 4975 2525 2    50   ~ 0
XSHUT
Text Label 4975 2425 2    50   ~ 0
LX
Text Label 4975 2625 2    50   ~ 0
MPP-SET
Text Label 9100 2275 2    50   ~ 0
VBAT+
Wire Wire Line
	10500 5850 10500 5800
$Comp
L power:GND #PWR0101
U 1 1 5DB42604
P 10500 5850
F 0 "#PWR0101" H 10500 5600 50  0001 C CNN
F 1 "GND" H 10505 5677 50  0000 C CNN
F 2 "" H 10500 5850 50  0001 C CNN
F 3 "" H 10500 5850 50  0001 C CNN
	1    10500 5850
	1    0    0    -1  
$EndComp
$Comp
L power:PWR_FLAG #FLG0101
U 1 1 5DB417EC
P 10500 5800
F 0 "#FLG0101" H 10500 5875 50  0001 C CNN
F 1 "PWR_FLAG" H 10500 5973 50  0000 C CNN
F 2 "" H 10500 5800 50  0001 C CNN
F 3 "~" H 10500 5800 50  0001 C CNN
	1    10500 5800
	1    0    0    -1  
$EndComp
$Comp
L Device:CP1 COUT2
U 1 1 5DA39F62
P 8025 1675
F 0 "COUT2" H 8140 1721 50  0000 L CNN
F 1 "33uF" H 8140 1630 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 8025 1675 50  0001 C CNN
F 3 "~" H 8025 1675 50  0001 C CNN
F 4 "C2012X5R1A336M125AC" H 8025 1675 50  0001 C CNN "MPN"
	1    8025 1675
	1    0    0    -1  
$EndComp
$Comp
L Device:CP1 COUT1
U 1 1 5DB2B991
P 7525 1675
F 0 "COUT1" H 7640 1721 50  0000 L CNN
F 1 "33uF" H 7640 1630 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 7525 1675 50  0001 C CNN
F 3 "~" H 7525 1675 50  0001 C CNN
F 4 "C2012X5R1A336M125AC" H 7525 1675 50  0001 C CNN "MPN"
	1    7525 1675
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0102
U 1 1 5DB2C93F
P 7525 1925
F 0 "#PWR0102" H 7525 1675 50  0001 C CNN
F 1 "GND" H 7530 1752 50  0000 C CNN
F 2 "" H 7525 1925 50  0001 C CNN
F 3 "" H 7525 1925 50  0001 C CNN
	1    7525 1925
	1    0    0    -1  
$EndComp
Wire Wire Line
	7025 2275 7025 1425
Wire Wire Line
	7025 1425 7525 1425
Wire Wire Line
	7525 1425 7525 1525
Wire Wire Line
	7525 1825 7525 1925
Wire Wire Line
	7525 1425 8025 1425
Wire Wire Line
	8025 1425 8025 1525
Connection ~ 7525 1425
Wire Wire Line
	8025 1825 8025 1925
$Comp
L power:GND #PWR0103
U 1 1 5DD5E8A3
P 8025 1925
F 0 "#PWR0103" H 8025 1675 50  0001 C CNN
F 1 "GND" H 8030 1752 50  0000 C CNN
F 2 "" H 8025 1925 50  0001 C CNN
F 3 "" H 8025 1925 50  0001 C CNN
	1    8025 1925
	1    0    0    -1  
$EndComp
$Comp
L Device:Solar_Cell SC1
U 1 1 5DD659AE
P 2600 4925
F 0 "SC1" H 2708 5021 50  0000 L CNN
F 1 "Solar_Cell" H 2708 4930 50  0000 L CNN
F 2 "lsf-kicad-lib:SM141K04LV" V 2600 4985 50  0001 C CNN
F 3 "~" V 2600 4985 50  0001 C CNN
F 4 "SM141K04LV" H 2600 4925 50  0001 C CNN "MPN"
	1    2600 4925
	1    0    0    -1  
$EndComp
$Comp
L Device:Solar_Cell SC2
U 1 1 5DD682BA
P 3350 4925
F 0 "SC2" H 3458 5021 50  0000 L CNN
F 1 "Solar_Cell" H 3458 4930 50  0000 L CNN
F 2 "lsf-kicad-lib:SM141K04LV" V 3350 4985 50  0001 C CNN
F 3 "~" V 3350 4985 50  0001 C CNN
F 4 "SM141K04LV" H 3350 4925 50  0001 C CNN "MPN"
	1    3350 4925
	1    0    0    -1  
$EndComp
$Comp
L Device:Solar_Cell SC3
U 1 1 5DD68A43
P 4050 4925
F 0 "SC3" H 4158 5021 50  0000 L CNN
F 1 "Solar_Cell" H 4158 4930 50  0000 L CNN
F 2 "lsf-kicad-lib:SM141K04LV" V 4050 4985 50  0001 C CNN
F 3 "~" V 4050 4985 50  0001 C CNN
F 4 "SM141K04LV" H 4050 4925 50  0001 C CNN "MPN"
	1    4050 4925
	1    0    0    -1  
$EndComp
Wire Wire Line
	2600 4725 2600 4600
Wire Wire Line
	2600 4600 3350 4600
Wire Wire Line
	2600 5025 2600 5150
Wire Wire Line
	2600 5150 3350 5150
Wire Wire Line
	4050 4725 4050 4600
Connection ~ 4050 4600
Wire Wire Line
	4050 4600 4350 4600
Wire Wire Line
	3350 4725 3350 4600
Connection ~ 3350 4600
Wire Wire Line
	3350 4600 4050 4600
Wire Wire Line
	3350 5025 3350 5150
Connection ~ 3350 5150
Wire Wire Line
	3350 5150 4050 5150
Wire Wire Line
	4050 5025 4050 5150
Text Label 4350 4600 0    50   ~ 0
PV+
Text Label 4350 5150 0    50   ~ 0
PV-
Wire Wire Line
	5975 2875 6275 2875
Wire Wire Line
	7625 2875 7625 3050
Wire Wire Line
	6275 3050 6275 2875
Connection ~ 6275 2875
Wire Wire Line
	6275 2875 7625 2875
Wire Wire Line
	4775 2875 4725 2875
Wire Wire Line
	4725 3050 4725 2875
Connection ~ 4725 2875
Wire Wire Line
	4725 2875 3575 2875
Wire Wire Line
	4725 3350 4725 3575
Wire Wire Line
	6275 3350 6275 3575
Wire Wire Line
	7625 3350 7625 3575
Wire Wire Line
	8525 3000 8525 3575
$Comp
L power:GND #PWR0104
U 1 1 5DDA9211
P 4725 3575
F 0 "#PWR0104" H 4725 3325 50  0001 C CNN
F 1 "GND" H 4730 3402 50  0000 C CNN
F 2 "" H 4725 3575 50  0001 C CNN
F 3 "" H 4725 3575 50  0001 C CNN
	1    4725 3575
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0105
U 1 1 5DDA956E
P 6275 3575
F 0 "#PWR0105" H 6275 3325 50  0001 C CNN
F 1 "GND" H 6280 3402 50  0000 C CNN
F 2 "" H 6275 3575 50  0001 C CNN
F 3 "" H 6275 3575 50  0001 C CNN
	1    6275 3575
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0106
U 1 1 5DDA993E
P 7625 3575
F 0 "#PWR0106" H 7625 3325 50  0001 C CNN
F 1 "GND" H 7630 3402 50  0000 C CNN
F 2 "" H 7625 3575 50  0001 C CNN
F 3 "" H 7625 3575 50  0001 C CNN
	1    7625 3575
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0107
U 1 1 5DDA9D20
P 8525 3575
F 0 "#PWR0107" H 8525 3325 50  0001 C CNN
F 1 "GND" H 8530 3402 50  0000 C CNN
F 2 "" H 8525 3575 50  0001 C CNN
F 3 "" H 8525 3575 50  0001 C CNN
	1    8525 3575
	1    0    0    -1  
$EndComp
$Comp
L Device:CP1 CIN2
U 1 1 5DDAFE8B
P 2775 2825
F 0 "CIN2" H 2890 2871 50  0000 L CNN
F 1 "33uF" H 2890 2780 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 2775 2825 50  0001 C CNN
F 3 "~" H 2775 2825 50  0001 C CNN
F 4 "C2012X5R1A336M125AC" H 2775 2825 50  0001 C CNN "MPN"
	1    2775 2825
	1    0    0    -1  
$EndComp
Wire Wire Line
	2775 2975 2775 3575
Wire Wire Line
	3175 2975 3175 3575
$Comp
L power:GND #PWR0108
U 1 1 5DDB6E82
P 3175 3575
F 0 "#PWR0108" H 3175 3325 50  0001 C CNN
F 1 "GND" H 3180 3402 50  0000 C CNN
F 2 "" H 3175 3575 50  0001 C CNN
F 3 "" H 3175 3575 50  0001 C CNN
	1    3175 3575
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0109
U 1 1 5DDB71FA
P 2775 3575
F 0 "#PWR0109" H 2775 3325 50  0001 C CNN
F 1 "GND" H 2780 3402 50  0000 C CNN
F 2 "" H 2775 3575 50  0001 C CNN
F 3 "" H 2775 3575 50  0001 C CNN
	1    2775 3575
	1    0    0    -1  
$EndComp
Wire Wire Line
	2775 2675 2775 2275
Wire Wire Line
	2525 2275 2775 2275
Connection ~ 2775 2275
Wire Wire Line
	2775 2275 3175 2275
Wire Wire Line
	9100 2275 8525 2275
Connection ~ 8525 2275
$Comp
L power:GND #PWR0111
U 1 1 5DDC9CAD
P 4350 5250
F 0 "#PWR0111" H 4350 5000 50  0001 C CNN
F 1 "GND" H 4355 5077 50  0000 C CNN
F 2 "" H 4350 5250 50  0001 C CNN
F 3 "" H 4350 5250 50  0001 C CNN
	1    4350 5250
	1    0    0    -1  
$EndComp
Wire Wire Line
	4350 5150 4350 5250
Wire Wire Line
	4050 5150 4350 5150
Connection ~ 4050 5150
Text Label 2525 2275 0    50   ~ 0
PV+
$Comp
L Connector_Generic:Conn_01x02 J1
U 1 1 5DD6F6CC
P 9300 2275
F 0 "J1" H 9380 2267 50  0000 L CNN
F 1 "Conn_01x02" H 9380 2176 50  0000 L CNN
F 2 "Connector_JST:JST_SH_BM02B-SRSS-TB_1x02-1MP_P1.00mm_Vertical" H 9300 2275 50  0001 C CNN
F 3 "~" H 9300 2275 50  0001 C CNN
F 4 "BM02B-SRSS-TB(LF)(SN) " H 9300 2275 50  0001 C CNN "MPN"
	1    9300 2275
	1    0    0    -1  
$EndComp
Wire Wire Line
	9100 2375 8975 2375
Wire Wire Line
	8975 2375 8975 3575
$Comp
L power:GND #PWR0110
U 1 1 5DD72C32
P 8975 3575
F 0 "#PWR0110" H 8975 3325 50  0001 C CNN
F 1 "GND" H 8980 3402 50  0000 C CNN
F 2 "" H 8975 3575 50  0001 C CNN
F 3 "" H 8975 3575 50  0001 C CNN
	1    8975 3575
	1    0    0    -1  
$EndComp
Wire Wire Line
	4775 2875 4775 2625
Wire Wire Line
	4775 2625 4975 2625
Wire Wire Line
	4625 2275 4775 2275
Wire Wire Line
	5475 3075 5475 3025
Wire Wire Line
	5975 2425 6375 2425
Wire Wire Line
	6225 2275 6225 2325
Wire Wire Line
	6225 2325 5975 2325
Wire Wire Line
	5975 2875 5975 2725
Wire Wire Line
	3575 2525 4975 2525
Wire Wire Line
	4775 2275 4775 2425
Wire Wire Line
	4775 2425 4975 2425
Text Notes 2025 2875 0    50   ~ 0
CIN2 is optional
Text Notes 7700 3450 0    50   ~ 0
Other option is 442k\nP/N: RT0603BRD07442KL 
Text Notes 8425 1700 0    50   ~ 0
COUT2 is optional
Text Notes 3825 1975 0    50   ~ 0
Other option is 47uH 20% 1.2A\nP/N: SRN6045TA-470M 
$Comp
L power:PWR_FLAG #FLG0102
U 1 1 5E069B59
P 4775 2275
F 0 "#FLG0102" H 4775 2350 50  0001 C CNN
F 1 "PWR_FLAG" H 4775 2448 50  0000 C CNN
F 2 "" H 4775 2275 50  0001 C CNN
F 3 "~" H 4775 2275 50  0001 C CNN
	1    4775 2275
	1    0    0    -1  
$EndComp
Connection ~ 4775 2275
$Comp
L Device:D_Zener D_OUT1
U 1 1 5DA3AEAA
P 8525 2850
F 0 "D_OUT1" V 8479 2929 50  0000 L CNN
F 1 "4v5" V 8570 2929 50  0000 L CNN
F 2 "lsf-kicad-lib:DO-222AA" H 8525 2850 50  0001 C CNN
F 3 "~" H 8525 2850 50  0001 C CNN
F 4 "SMM4F5.0A-TR" H 8525 2850 50  0001 C CNN "MPN"
	1    8525 2850
	0    1    1    0   
$EndComp
$EndSCHEMATC
